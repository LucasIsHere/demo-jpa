package org.example.demo;

import org.example.entity.manyToMany.Post;
import org.example.entity.manyToMany.Tag;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Demo2 {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa_demo");

    public static void main() {

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Post post1 = new Post("Post 1");
        Post post2 = new Post("Post 2");

        Tag tag1 = new Tag();
        tag1.setName("Tag 1");
        Tag tag2 = new Tag();
        tag2.setName("Tag 2");

        post1.addTag(tag1);
        post1.addTag(tag2);
        post2.addTag(tag2);

        em.persist(post1);
        em.persist(post2);

        em.getTransaction().commit();
        em.close();
        emf.close();
    }
}
