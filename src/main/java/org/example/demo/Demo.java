package org.example.demo;

import org.example.entity.oneToMany.Group;
import org.example.entity.oneToMany.User;
import org.example.entity.oneToOne.Address;
import org.example.entity.oneToOne.House;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class Demo {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa_demo");

    public static void main() {

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

//        Address address = new Address();
//        address.setNumero(380);
//        address.setNomRue("rue Saint-Pierre");
//        address.setCodePostal("59190");
//        address.setVille("Hondeghem");
//        address.setLongueur(750);
//
//        House house = new House();
//        house.setTaille(20);
//        house.setAdresse(address);
//
//        em.persist(address);
//        em.persist(house);

        House house = em.find(House.class, 1L);
        System.out.println(house);

        em.getTransaction().commit();
        em.close();
        emf.close();
    }

    public static void main2() {

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

//        User user = new User();
//        user.setUsername("Tristan");
//        user.setPassword("1234");
//
//        Group group = new Group();
//        group.setName("Autistes");
//
//        user.setGroup(group);
//        group.getUsers().add(user);
//
//        em.persist(user);
//        em.persist(group);

        Group group = new Group();
        group = em.find(Group.class, 1L);
        System.out.println(group);

        em.getTransaction().commit();
        em.close();
        emf.close();
    }

    public static void remove() {

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        User user = em.find(User.class, 1L);
        em.remove(user);

        em.getTransaction().commit();
        em.close();
        emf.close();
    }

    public static void nativeQuery() {

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        User user = new User();
        user.setUsername("Abdallah");
        user.setPassword("56789");
        em.persist(user);

        System.out.println(
                em.createNativeQuery("select * from user2 where username = ?", User.class)
                        .setParameter(1, "Abdallah").getFirstResult()
        );

        em.getTransaction().commit();
        em.close();
        emf.close();
    }

    public static void createQuery() {

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        User user = new User();
        user.setUsername("Jean-Louis");
        user.setPassword("123456");

        em.persist(user);

        List<User> listUser = null;

        listUser = em.createQuery("select u from User u", User.class).getResultList();
        listUser.forEach(System.out::println);

        em.getTransaction().commit();
        em.close();
        emf.close();
    }

    public static void createQuery2() {

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        User user = new User();

        em.createQuery("select u from User u where id = :id", User.class)
                .setParameter("id", 2L).getSingleResult();

        System.out.println(user);

        Long result = (Long) em.createQuery("select count(u) from User u").getSingleResult();

        System.out.println(result);

        em.getTransaction().commit();
        em.close();
        emf.close();
    }

    public static void createQuery3() {

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.createQuery("delete from User u where u.id = :id")
                .setParameter("id", 3L).executeUpdate();

        em.getTransaction().commit();
        em.close();
        emf.close();
    }
}
