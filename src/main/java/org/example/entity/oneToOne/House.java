package org.example.entity.oneToOne;

import javax.persistence.*;

@Entity
@Table(name = "maison")
public class House {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "maison_id")
    private Long id;
    private Integer taille;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "adresse_id", referencedColumnName = "id_adresse")
    private Address adresse;

    public House() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTaille() {
        return taille;
    }

    public void setTaille(Integer taille) {
        this.taille = taille;
    }

    public Address getAdresse() {
        return adresse;
    }

    public void setAdresse(Address adresse) {
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return "House{" +
                "id=" + id +
                ", taille=" + taille +
                ", adresse=" + adresse +
                '}';
    }
}
