create database demo_bdd_jpa;
use demo_bdd_jpa;

create table adresse(
id_adresse int primary key auto_increment,
numero int not null,
nom_rue varchar(100) not null,
code_postal varchar(5) not null,
ville varchar(50)
);

create table maison(
maison_id int primary key auto_increment,
taille int,
adresse_id int,
foreign key(adresse_id) references adresse(id_adresse)
);

select * from adresse;
select * from maison;

create table user2(
id int primary key auto_increment,
username varchar(100),
password varchar(100),
createTime date,
id_group int,
foreign key(id_group) references group2(id)
);

create table group2(
id int primary key auto_increment,
name varchar(100)
);

select * from user2;
select * from group2;

create table post(
id int primary key auto_increment,
title varchar(50)
);

create table tag(
id int primary key auto_increment,
name varchar(50)
);

create table post_tag(
post_id int not null,
tag_id int not null,
primary key(post_id, tag_id),
foreign key(post_id) references post(id) on update cascade,
foreign key(tag_id) references tag(id) on update cascade
);

select * from post_tag;
